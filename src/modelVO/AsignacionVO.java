/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelVO;

/**
 *
 * @author Gabriel Jaimes
 */
public class AsignacionVO {
    
    Integer codigo;
    Integer cod_cliente;
    Integer cod_clase;
    String fecha_inicio;
    String fecha_final;

    public AsignacionVO() {
    }

    public AsignacionVO(Integer codigo, Integer cod_cliente, Integer cod_clase, String fecha_inicio, String fecha_final) {
        this.codigo = codigo;
        this.cod_cliente = cod_cliente;
        this.cod_clase = cod_clase;
        this.fecha_inicio = fecha_inicio;
        this.fecha_final = fecha_final;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getCod_cliente() {
        return cod_cliente;
    }

    public void setCod_cliente(Integer cod_cliente) {
        this.cod_cliente = cod_cliente;
    }

    public Integer getCod_clase() {
        return cod_clase;
    }

    public void setCod_clase(Integer cod_clase) {
        this.cod_clase = cod_clase;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getFecha_final() {
        return fecha_final;
    }

    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    @Override
    public String toString() {
        return "AsignacionVO{" + "codigo=" + codigo + ", cod_cliente=" + cod_cliente + ", cod_clase=" + cod_clase + ", fecha_inicio=" + fecha_inicio + ", fecha_final=" + fecha_final + '}';
    }
    
    
    
}
