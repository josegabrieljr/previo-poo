/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelVO;

/**
 *
 * @author Gabriel Jaimes
 */
public class ClasesVO {
    Integer codigo;
    String clase;
    String horario;
    Integer cod_instructor;

    public ClasesVO() {
    }

    public ClasesVO(Integer codigo, String clase, String horario, Integer cod_instructor) {
        this.codigo = codigo;
        this.clase = clase;
        this.horario = horario;
        this.cod_instructor = cod_instructor;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Integer getCod_instructor() {
        return cod_instructor;
    }

    public void setCod_instructor(Integer cod_instructor) {
        this.cod_instructor = cod_instructor;
    }

    @Override
    public String toString() {
        return "ClasesVO{" + "codigo=" + codigo + ", clase=" + clase + ", horario=" + horario + ", cod_instructor=" + cod_instructor + '}';
    }
    
    
}
