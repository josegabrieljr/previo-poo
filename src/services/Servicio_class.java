/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelVO.ClasesVO;
import modelVO.ClienteVO;

/**
 *
 * @author Gabriel Jaimes
 */
public class Servicio_class {

    Conexion cnx;

    public Servicio_class() {
        cnx = new Conexion();
    }

    private void resgistrar(ClasesVO datos) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "INSERT INTO clase (codigo,clase,horario,cod_instructor) VALUES (?,?,?,?)";
        // se envian los datos a la consulta SQL
        ps = cx.prepareStatement(sql);
        ps.setInt(1, datos.getCodigo());
        ps.setString(2, datos.getClase());
        ps.setString(3, datos.getHorario());
        ps.setInt(4, datos.getCod_instructor());
        // se ejecuta la consulta SQL
        ps.executeUpdate();
        // se cierra la base de datos
        cx.close();
        ps.close();
    }

    /**
     *
     * @param datos
     * @throws Exception
     */
    public void setRegistrar(ClasesVO datos) throws Exception // decorador
    {
        this.resgistrar(datos);
    }

    private void eliminarCliente(Integer codigo) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "DELETE FROM clase WHERE codigo=?";
        // se envian los datos a la consulta SQL
        ps = cx.prepareCall(sql);
        ps.setInt(1, codigo);
        // se ejecuta la consulta SQL
        ps.execute();
        // se cierra la base de datos
        cx.close();
        ps.close();
    }

    public void setEliminar(Integer cedula) throws Exception // decorador
    {
        this.eliminarCliente(cedula);
    }

    private void modificar(ClasesVO datos, Integer codigo) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "UPDATE clase SET codigo=?,clase=?,horario=?,cod_instructor=? WHERE codigo=?";
        // se envian los datos a la consulta SQL
        ps = cx.prepareCall(sql);
        ps.setInt(1, datos.getCodigo());
        ps.setString(2, datos.getClase());
        ps.setString(3, datos.getHorario());
        ps.setInt(4, datos.getCod_instructor());
        ps.setInt(5, codigo);
        // se ejecuta la consulta SQL
        ps.executeUpdate();
        // se cierra la base de datos
        cx.close();
        ps.close();

    }

    public void setModificar(ClasesVO datos, Integer codigo) throws Exception// decorador
    {
        this.modificar(datos, codigo);
    }

    private ClasesVO buscar(String codigo) throws SQLException {
        // SE REALIZA LA CONEXION A LA BASE DE DATOS
        PreparedStatement ps;
        Connection cx = cnx.getConexion();;
        ResultSet rta = null;
        // se prepara la consulta sql
        String sql = "SELECT * FROM clase";
        // se ejecuta la consulta sql 
        ps = cx.prepareStatement(sql);
        rta = ps.executeQuery();
        ClasesVO cliente = new ClasesVO();
        while (rta.next()) {
            if (rta.getString(1).equals(codigo)) {
                cliente.setCodigo(rta.getInt(1));
                cliente.setClase(rta.getString(2));
                cliente.setHorario(rta.getString(3));
                cliente.setCod_instructor(rta.getInt(4));
                break;
            }
        }
        return cliente;

    }

    public ClasesVO buscar2(String dato) throws Exception // decorador
    {
        return this.buscar(dato);
    }

    private DefaultComboBoxModel comboBox() throws SQLException {
         // SE REALIZA LA CONEXION A LA BASE DE DATOS
        PreparedStatement ps;
        Connection cx = cnx.getConexion();;
        ResultSet rta = null;
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        // se prepara la consulta sql
        String sql = "SELECT Codigo FROM clase ORDER BY codigo";
        // se ejecuta la consulta sql 
        ps = cx.prepareStatement(sql);
        rta = ps.executeQuery();
        while (rta.next()) {
            modelo.addElement(rta.getString("codigo"));
        }
        return modelo;
    }

    public DefaultComboBoxModel getComboBox() throws SQLException {

        return this.comboBox();
    }
}
