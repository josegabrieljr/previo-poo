/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import modelVO.AsignacionVO;

/**
 *
 * @author Gabriel Jaimes
 */
public class Servicio_Asignacion {

    Conexion cnx;

    public Servicio_Asignacion() {
        cnx = new Conexion();
    }

    public void insertarAsignacion(AsignacionVO datos) throws SQLException {

        // se conecta con la base de datos
        PreparedStatement ps;
        Connection cs = cnx.getConexion();
        // se prepara la consulta sql
        String sql = "INSERT INTO asignacion VALUES (?,?,?,?,?)";

        ps = cs.prepareStatement(sql);
        // se insertan los datos
        ps.setInt(1, datos.getCodigo());
        ps.setInt(2, datos.getCod_cliente());
        ps.setInt(3, datos.getCod_clase());
        ps.setString(4, datos.getFecha_inicio());
        ps.setString(5, datos.getFecha_final());
        // se ejecuta
        ps.executeUpdate();
      //  System.out.println(datos.toString());

        // se cierra las coneciones
        cs.close();
        ps.close();
    }

    private AsignacionVO buscar(String codigo) {
        // se conecta a la base de datos    
        PreparedStatement ps;
        Connection cs = cnx.getConexion();
        AsignacionVO model = new AsignacionVO();
        ResultSet rta;
        // consulta sql;
        String sql = "select * from asignacion where codigo=?";
        try {
            ps = cs.prepareStatement(sql);

            ps.setInt(1, Integer.parseInt(codigo));
            rta = ps.executeQuery();
            while (rta.next()) {
                model.setCodigo(rta.getInt("codigo"));
                model.setCod_cliente(rta.getInt("cliente"));
                model.setCod_clase(rta.getInt("clase"));
                model.setFecha_inicio(rta.getString("fecha_ini"));
                model.setFecha_final(rta.getString("fecha_fin"));
            }
            return model;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public AsignacionVO getBusqueda(String codigo) throws SQLException {

        return this.buscar(codigo);
    }

    private void modificar(AsignacionVO datos, String codigo) {
        PreparedStatement ps;
        Connection cx = cnx.getConexion();

        String Sql = "update asignacion set codigo=?,cliente=?,clase=?,fecha_ini=?,fecha_fin=? where codigo=?";
        try {
            ps = cx.prepareStatement(Sql);
            ps.setInt(1, datos.getCodigo());
            ps.setInt(2, datos.getCod_cliente());
            ps.setInt(3, datos.getCod_clase());
            ps.setString(4, datos.getFecha_inicio());
            ps.setString(5, datos.getFecha_final());
            ps.setString(6, codigo);
            ps.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void setModificar(AsignacionVO datos, String codigo) {
        this.modificar(datos, codigo);
    }

    private void eliminar(String codigo) {
        PreparedStatement ps;
        Connection cx = cnx.getConexion();

        String sql = "delect from asignacion where codigo=?";

        try {
            ps = cx.prepareStatement(sql);
            ps.setString(1, codigo);
            ps.execute();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void setEliminar(String codigo) {
     this.eliminar(codigo);
    }
}
