/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelVO.ClienteVO;
import modelVO.InstructorVO;

/**
 *
 * @author Gabriel Jaimes
 */
public class Servicio_Instructor {

    Conexion cnx;

    public Servicio_Instructor() {
        cnx = new Conexion();
    }

    private void resgistrar(InstructorVO datos) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "INSERT INTO instructor (codigo,nombre,apellido,telefono,especialidad,jornada,hora) VALUES (?,?,?,?,?,?,?)";
        // se envian los datos a la consulta SQL
        ps = cx.prepareStatement(sql);
        ps.setInt(1, datos.getCodigo());
        ps.setString(2, datos.getNombre());
        ps.setString(3, datos.getApellidos());
        ps.setString(4, datos.getTelefono());
        ps.setString(5, datos.getEspecialidad());
        ps.setString(6, datos.getJornada());
        ps.setString(7, datos.getHora());
        // se ejecuta la consulta SQL
        ps.execute();
        // se cierra la base de datos
        cx.close();
        ps.close();
    }

    /**
     *
     * @param datos
     * @throws Exception
     */
    public void setRegistrar(InstructorVO datos) throws Exception // decorador
    {
        this.resgistrar(datos);
    }

    private void eliminarCliente(Integer codigo) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "DELETE FROM instructor WHERE codigo=?";
        // se envian los datos a la consulta SQL
        ps = cx.prepareCall(sql);
        ps.setInt(1, codigo);
        // se ejecuta la consulta SQL
        ps.execute();
        // se cierra la base de datos
        cx.close();
        ps.close();
    }

    public void setEliminar(Integer codigo) throws Exception // decorador
    {
        this.eliminarCliente(codigo);
    }

    private void modificar(InstructorVO datos, Integer codigo) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "UPDATE instructor SET codigo=?,nombre=?,apellido=?,telefono=?,especialidad=?,jornada=?,hora=? WHERE codigo=?";
        // se envian los datos a la consulta SQL
        ps = cx.prepareCall(sql);
        ps.setInt(1, datos.getCodigo());
        ps.setString(2, datos.getNombre());
        ps.setString(3, datos.getApellidos());
        ps.setString(4, datos.getTelefono());
        ps.setString(5, datos.getEspecialidad());
        ps.setString(6, datos.getJornada());
        ps.setString(7, datos.getHora());
        ps.setInt(8, codigo);
        // se ejecuta la consulta SQL
        ps.executeUpdate();
        // se cierra la base de datos
        cx.close();
        ps.close();

    }

    public void setModificar(InstructorVO datos, Integer codigo) throws Exception// decorador
    {
        this.modificar(datos, codigo);
    }

    private InstructorVO buscar(String codigo) throws SQLException {
        // SE REALIZA LA CONEXION A LA BASE DE DATOS
        PreparedStatement ps;
        Connection cx = cnx.getConexion();;
        ResultSet rta = null;
        InstructorVO instructor = new InstructorVO();
        // se prepara la consulta sql
        String sql = "SELECT * FROM instructor";
        // se ejecuta la consulta sql 
        ps = cx.prepareStatement(sql);
        rta = ps.executeQuery();
        while (rta.next()) {
            if (rta.getString(1).equals(codigo)) {
                instructor.setCodigo(rta.getInt(1));
                instructor.setNombre(rta.getString(2));
                instructor.setApellidos(rta.getString(3));
                instructor.setTelefono(rta.getString(4));
                instructor.setEspecialidad(rta.getString(5));
                instructor.setJornada(rta.getString(6));
                instructor.setHora(rta.getString(7));
                break;
            }

        }

        return instructor;
    }

    public InstructorVO buscar2(String dato) throws Exception // decorador
    {
        return this.buscar(dato);
    }

    private DefaultComboBoxModel buscar() throws SQLException {
        // SE REALIZA LA CONEXION A LA BASE DE DATOS
        PreparedStatement ps;
        Connection cx = cnx.getConexion();
        ResultSet rta = null;
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        // String[] instructor ;
        // se prepara la consulta sql
        String sql = "SELECT Codigo FROM instructor ORDER BY codigo";
        // se ejecuta la consulta sql 
        ps = cx.prepareStatement(sql);
        rta = ps.executeQuery();
        // instructor = new String[1];
        while (rta.next()) {
            modelo.addElement(rta.getString("codigo"));
        }
        return modelo;

    }

    public DefaultComboBoxModel comboBox() throws Exception // decorador
    {
        return this.buscar();

    }
}
