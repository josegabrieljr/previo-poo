/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Gabriel Jaimes
 */
public class Conexion  {
    private static  final String url="jdbc:mysql://localhost:3306/gimnasio";
    private static  final String userName= "root";
    private static  final String password= "";
    
    
    public static Connection getConexion(){
      Connection cnx = null;
    
        try {
            cnx= DriverManager.getConnection(url,userName,password);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"No se pudo obtener conexion a la base de datos");
        }
    return cnx;
    }
}
   