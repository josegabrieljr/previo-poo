/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelVO.ClienteVO;

/**
 *
 * @author Gabriel Jaimes
 */
public class Servicio_Cliente {

    Conexion cnx;

    public Servicio_Cliente() {
        cnx = new Conexion();
    }

    private void resgistrar(ClienteVO datos) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "INSERT INTO cliente (cedula,nombre,apellido,direccion,telefono,fecha) VALUES (?,?,?,?,?,?)";
        // se envian los datos a la consulta SQL
        ps = cx.prepareStatement(sql);
        ps.setInt(1, datos.getCedula());
        ps.setString(2, datos.getNombre());
        ps.setString(3, datos.getApellidos());
        ps.setString(4, datos.getDireccion());
        ps.setString(5, datos.getTelefono());
        ps.setString(6, datos.getFechaIngreso());
        // se ejecuta la consulta SQL
        ps.executeUpdate();
        // se cierra la base de datos
        cx.close();
        ps.close();
    }

    /**
     *
     * @param datos
     * @throws Exception
     */
    public void setRegistrar(ClienteVO datos) throws Exception // decorador
    {
        this.resgistrar(datos);
    }

    private void eliminarCliente(Integer cedula) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "DELETE FROM cliente WHERE cedula=?";
        // se envian los datos a la consulta SQL
        ps = cx.prepareCall(sql);
        ps.setInt(1, cedula);
        // se ejecuta la consulta SQL
        ps.execute();
        // se cierra la base de datos
        cx.close();
        ps.close();
    }

    public void setEliminar(Integer cedula) throws Exception // decorador
    {
        this.eliminarCliente(cedula);
    }

    private void modificar(ClienteVO datos, Integer cedula) throws SQLException {
        // se conecta en la base de datos
        PreparedStatement ps = null;
        Connection cx = cnx.getConexion();
        // se realiza un prepara la consulta SQL
        String sql = "UPDATE cliente SET cedula=?,nombre=?,apellido=?,direccion=?,telefono=?,fecha=? WHERE cedula=?";
        // se envian los datos a la consulta SQL
        ps = cx.prepareCall(sql);
        ps.setInt(1, datos.getCedula());
        ps.setString(2, datos.getNombre());
        ps.setString(3, datos.getApellidos());
        ps.setString(4, datos.getDireccion());
        ps.setString(5, datos.getTelefono());
        ps.setString(6, datos.getFechaIngreso());
        ps.setInt(7, cedula);
        // se ejecuta la consulta SQL
        ps.executeUpdate();
        // se cierra la base de datos
        cx.close();
        ps.close();

    }

    public void setModificar(ClienteVO datos, Integer cedula) throws Exception// decorador
    {
        this.modificar(datos, cedula);
    }

    private ClienteVO buscar(Integer cedula) throws SQLException {
        // SE REALIZA LA CONEXION A LA BASE DE DATOS
        PreparedStatement ps;
        Connection cx = cnx.getConexion();;
        ResultSet rta = null;
        // se prepara la consulta sql
        String sql = "SELECT * FROM instructor";
        // se ejecuta la consulta sql 
        ps = cx.prepareStatement(sql);
        rta = ps.executeQuery();
        System.out.println("bien");
        ClienteVO cliente = new ClienteVO();
        while (rta.next()) {
            if (rta.getString(1).equals(cedula.toString())) {
                cliente.setCedula(Integer.parseInt(rta.getString(1)));
                cliente.setNombre(rta.getString(2));
                cliente.setApellidos(rta.getString(3));
                cliente.setDireccion(rta.getString(4));
                cliente.setTelefono(rta.getString(5));
                cliente.setFechaIngreso(rta.getString(6));
                break;
            }
            return cliente;
        }
        return null;

    }

    public ClienteVO buscar2(Integer dato) throws Exception // decorador
    {
        return this.buscar(dato);
    }

    private DefaultComboBoxModel comboBox() throws SQLException {
         // SE REALIZA LA CONEXION A LA BASE DE DATOS
        PreparedStatement ps;
        Connection cx = cnx.getConexion();;
        ResultSet rta = null;
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        // se prepara la consulta sql
        String sql = "SELECT Cedula FROM cliente";
        // se ejecuta la consulta sql 
        ps = cx.prepareStatement(sql);
        rta = ps.executeQuery();
        // instructor = new String[1];
        while (rta.next()) {
            modelo.addElement(rta.getString("cedula"));
        }
        return modelo;

    }

    public DefaultComboBoxModel getComboBox() throws SQLException {

        return this.comboBox();
    }
}
