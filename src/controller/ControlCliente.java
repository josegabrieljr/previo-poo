/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.toedter.calendar.JDateChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelVO.ClienteVO;
import modelVO.FuncionesXYZ;
import services.Servicio_Cliente;

/**
 *
 * @author Gabriel Jaimes
 */
public class ControlCliente {

    Servicio_Cliente serverCl;

    public ControlCliente() {
        serverCl = new Servicio_Cliente();

    }

    public void setDato(ClienteVO datos) {
        try {
            serverCl.setRegistrar(datos);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Verifique que este conectando bien a la base datos o " + "\n" + "los datos no son los correspondientes");
        }

    }

    public void removeDato(Integer datox) {
        try {
            serverCl.setEliminar(datox);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Verifique que este conectando bien a la base datos o " + "\n" + "el dato no esta registrado");

        }

    }

    public void setModificador(ClienteVO datos, Integer cedula) {
        try {
            serverCl.setModificar(datos, cedula);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Verifique que este conectando bien a la base datos o " + "\n" + "los datos no son los correspondientes");
        }
    }

    public ClienteVO clienteEncontrado(Integer cedula) {

        try {
            return (serverCl.buscar2(cedula));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Verifique que este conectando bien a la base datos o " + "\n" + "los datos no son los correspondientes");

        }

        return null;
    }

    public DefaultComboBoxModel comboBox() {
        try {
            return serverCl.getComboBox();
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(null,"no se encontraron datos");

        }
        return null;

    }
}
