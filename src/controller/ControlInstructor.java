/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelVO.InstructorVO;
import services.Servicio_Instructor;

/**
 *
 * @author Gabriel Jaimes
 */
public class ControlInstructor {
    Servicio_Instructor serverIn;

    public ControlInstructor() {
       serverIn = new Servicio_Instructor();
    }

    public void setDato(InstructorVO datos) 
    {
        try {
            serverIn.setRegistrar(datos);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"los datos no son los correspondientes");
        }
        

    }
    
    public void removeDato(Integer datox)
    {
        try {
            serverIn.setEliminar(datox);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"el dato no esta registrado");
        
        }
    
    }

    public void setModificador(InstructorVO datos,Integer codigo)
    {
        try {
             serverIn.setModificar(datos,codigo);
        } catch (Exception e) {
              JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"los datos no son los correspondientes");
        }
    }
    
    public InstructorVO clienteEncontrado(String codigo){
    
        try {
            return(serverIn.buscar2(codigo));
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"los datos no son los correspondientes");
        
        }
    
    return null;
    }
    
    public DefaultComboBoxModel instructor(){
    
        try {
            return(serverIn.comboBox());
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos");
        
        }
    
    return null;
    }
}
