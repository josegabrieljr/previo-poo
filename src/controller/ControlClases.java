/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelVO.ClasesVO;
import services.Servicio_class;

/**
 *
 * @author Gabriel Jaimes
 */
public class ControlClases {
    // servicios control
    Servicio_class serverCl;

    public ControlClases() {
        serverCl = new Servicio_class();
    }

    public void setDato(ClasesVO datos) 
    {
        try {
            serverCl.setRegistrar(datos);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"los datos no son los correspondientes");
        }
        

    }
    
    public void removeDato(Integer codigo)
    {
        try {
            serverCl.setEliminar(codigo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"el dato no esta registrado");
        
        }
    
    }

    public void setModificador(ClasesVO datos,Integer codigo)
    {
        try {
             serverCl.setModificar(datos,codigo);
        } catch (Exception e) {
              JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"los datos no son los correspondientes");
        }
    }
    
    public ClasesVO infoClaseEncontrada(String codigo){
    
        try {
            return(serverCl.buscar2(codigo));
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null,"Verifique que este conectando bien a la base datos o "+"\n"+"los datos no son los correspondientes");
        
        }
    
    return null;
    }
    
    public DefaultComboBoxModel getComboBox(){
    
        try {
            return(serverCl.getComboBox());
        } catch (Exception e) {
        }
        return null;
    
    
    }
}
