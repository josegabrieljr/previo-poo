/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import modelVO.AsignacionVO;
import services.Servicio_Asignacion;
import services.Servicio_Cliente;

/**
 *
 * @author Gabriel Jaimes
 */
public class ControlAsignacion {

    Servicio_Asignacion sa;
    Servicio_Cliente cliente;

    public ControlAsignacion() {
        sa = new Servicio_Asignacion();
        cliente = new Servicio_Cliente();
    }

    public void setDato(AsignacionVO datos) {
        try {
            sa.insertarAsignacion(datos);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error");
        }

    }

    public AsignacionVO getBusqueda(String codigo) {
        try {
            return sa.getBusqueda(codigo);
        } catch (Exception e) {
        }
        return null;

    }

    public void setModificador(AsignacionVO datos, String codigo) {
        sa.setModificar(datos, codigo);
    }

    public void setEliminar(String codigo) {
        sa.setEliminar(codigo);
    }
}
